package com.example.sb_graphql.query;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.sb_graphql.entity.User;
import com.example.sb_graphql.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserQuery implements GraphQLQueryResolver {

    private final UserRepository userRepository;

    public UserQuery(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public long countUsers() {
        return userRepository.count();
    }
}
