package com.example.sb_graphql.query;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.sb_graphql.entity.User;
import com.example.sb_graphql.repository.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserMutation implements GraphQLMutationResolver {

    private final UserRepository userRepository;

    public UserMutation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User newUser(String firstname) {
        return userRepository.save( new User()
                .setFirstname(firstname)
                .setLastname("test"));

    }

}
