package com.example.sb_graphql;

import com.example.sb_graphql.entity.User;
import com.example.sb_graphql.repository.UserRepository;
import graphql.schema.GraphQLObjectType;
import graphql.servlet.GraphQLObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SbGraphqlApplication {

    private final UserRepository userRepository;

    public SbGraphqlApplication(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SbGraphqlApplication.class, args);
    }

    @Bean
    public CommandLineRunner init() {
        return (args) -> {
            userRepository.save(new User()
                    .setFirstname("patrick")
                    .setLastname("Liepisn")
                    .setPhone("111")
                    .setEmail("liepins@liepins.de")
                    .setAge(35));

        };
    }

}

