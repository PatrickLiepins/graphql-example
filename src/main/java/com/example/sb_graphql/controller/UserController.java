package com.example.sb_graphql.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("users")
public class UserController {

    @GetMapping("test")
    public int test() {
        return HttpServletResponse.SC_OK;
    }
}
